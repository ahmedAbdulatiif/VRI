//
//  Utilities.swift
//  VRI
//
//  Created by Mostafa El_sayed on 8/15/17.
//  Copyright © 2017 TheGang. All rights reserved.
//
//

import UIKit
import SystemConfiguration
import CoreTelephony


let defaults = UserDefaults.standard
let screenSize: CGRect = UIScreen.main.bounds


struct TableCellsID {
   static var sessionInfo = "SessionInfoCell"
   static var networkFailuer = "NetworkErrDescCell"
   static var resource = "ResourceCell"
}
